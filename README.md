## rancher-deploy-k8s

1. Install kubectl:

```
### Linux ###
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client

### macOS ###
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```

2. Install rke: 

```
### Linux ###
curl -s https://api.github.com/repos/rancher/rke/releases/latest | grep download_url | grep amd64 | cut -d '"' -f 4 | wget -qi -
chmod +x rke_linux-amd64
sudo mv rke_linux-amd64 /usr/local/bin/rke
rke --version

### macOS ###
curl -s https://api.github.com/repos/rancher/rke/releases/latest | grep download_url | grep darwin-amd64 | cut -d '"' -f 4 | wget -qi -
chmod +x rke_darwin-amd64
sudo mv rke_darwin-amd64 /usr/local/bin/rke
rke --version
```

3. Install helm:

```
### Helm 3 ###
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

RKE Supported operating systems
RKE runs on almost any Linux OS with Docker installed. Rancher has been tested and is supported with:

Red Hat Enterprise Linux

Oracle Enterprise Linux

CentOS Linux

Ubuntu

RancherOS

# Using Ansible Playbook to provision Ubuntu server for K8s

Replace your ansible_host inside inventory.ini with your master and workers IPs 

This playbook will do:-

1- Update your Linux System

2- Install Supported version of Docker

3- Enable required Kernel modules

4- Disable swap and Modify sysctl entries


```
ansible-playbook -i inventory.ini site.yml
```

# Config RKE 

1. Adjust the emailaddress in yaml/letsencrypt-issuer.yaml
2. Adjust DOMAIN-NAME for Rancher UI in yaml/rancher-https.yaml and yaml/tls-rancher-ingress.yaml and make sure this domain point to any public IP for your worker nodes
3. Replace your hostname and IPs inside cluster.yml 
4. Replace public IP inside cluster.yml


Make cluster up

```
rke up config cluster.yml

export KUBECONFIG=./kube_config_cluster.yml

```

Save this file cluster.yml if you want to add/remove nodes or change configration like k8s version in futuer 


If you want to manage k8s from your PC

` copy kube_config_cluster.yml to your PC under $HOME/.kube/config `


Check list of nodes in the cluster.

` kubectl get nodes ` 


# Use Longhorn as CSI drive

Install iscsi for master and workers 

```
sudo apt-get install open-iscsi -y 
sudo systemctl start iscsid && sudo systemctl enable iscsid
```

crate namespace
` kubectl create namespace longhorn-system `

Add the Helm repository:
` helm repo add longhorn https://charts.longhorn.io `

Update your local Helm chart repository cache:
` helm repo update `

To install Helm chart:
```
helm install longhorn longhorn/longhorn --namespace longhorn-system

helm list --namespace longhorn-system
```

# END
Now you have cluster UP with 

1. cert-manager.yaml installed 
2. letsencrypt-issuer.yaml for installed
3. rancher-https.yaml for rancher UI installed
4. tls-rancher-ingress.yaml for rancher UI https installed 
5. Ingress installed in rke cluster.yml config settinge as it create Nginx services on all workers node so if you want publish any service use any worker node with port 40, 443 or create LB to make taffic for all workers so if one worker down traffic will go to other worker 
If you didn't have LB use any IP of any worker but this will not be High avaliblty 

------------------------------------------------------------------------
------------------------------------------------------------------------

## This secion for install ingress, Cert-manager, Rancher UI using helm
## Please didn't use this unless you create cluster without ingress, Cert-manager,ClusterIssuer ,Rancher UI  and want to install them manually 

# Install ingress # no need for it as we enable ingress inside RKE 

but this if you don't enable it inside rke cluster.yaml or if you want use it in any cluster didn't have ingress


helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress ingress-nginx/ingress-nginx -f helm/ingress-values.yaml

----------------------------------------------------------------


# Install Cert-Manager to manage certificates. # no need for it as we install Cert-Manager inside RKE 
but this if you don't enable it inside rke cluster.yaml or if you want use it in any cluster didn't have Cert-Manager

```
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl create namespace cert-manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.1/cert-manager.crds.yaml

#Adjust the emailaddress in helm/letsencrypt-issuer.yaml
kubectl apply -f helm/letsencrypt-issuer.yaml
helm install cert-manager --namespace cert-manager jetstack/cert-manager --version v0.14.1
```
----------------------------------------------------------------

# Install Rancher UI # no need for it as we install Rancher UI inside RKE 
but this if you don't enable it inside rke cluster.yaml or if you want use it in any cluster didn't have Rancher UI
```
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest


kubectl create ns cattle-system

helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=DOMAIN-NAME \
  --set ingress.tls.source=letsEncrypt \
  --set letsEncrypt.email=YOUR-EMAIL
```